let isTravelling = false;
let audio = new Audio("./Assets/opening.mp3");
let video = null;
audio.volume = 0;

// function handleWeirdAnim(){
//     console.log("BANGARANG");
//     if(!isTravelling){
//         audio.currentTime=26;
//         audio.play()
//         volumeUp();
//         const element = document.querySelector("#weird_anim")
//         element.style.animation = "rotating 20s linear 1";
//         isTravelling = true;
//         setTimeout(() => {
//             isTravelling = false;
//             element.style.animation = "";
//             volumeDown();
//         }, 20000);
//     }
// }

function handleVideoStart(){
    let video = document.querySelector("#nekomarch");
    let container = document.querySelector("#weird_container");
    if(!isTravelling){
        audio.currentTime = 0;
        audio.play();
        volumeUp(audio);
        video.play();
        volumeUp(video);
        container.style.transform="translateY(-120%)";
        isTravelling = true;
        setTimeout(() => {
            isTravelling = false;
            container.style.transform="";
            volumeDown(video);
            audio.currentTime = 0;
            audio.play();
            volumeUp(audio);
        }, 132000);
    }
}

function volumeUp(media){
    let volume = 0;
    media.volume = volume;
    const interval = window.setInterval(() => {
        volume += 0.1;
        if(volume >= 1){
            clearInterval(interval);
        }
        else {
            media.volume = volume;
        }
    }, 200);
}

function volumeDown(media){
    let volume = 1;
    let interval = window.setInterval(() => {
        media.volume = volume;
        volume -= 0.1;
        if(volume <= 0){
            clearInterval(interval);
            media.pause();
            media.currentTime = 0;
        }
    }, 300);
}

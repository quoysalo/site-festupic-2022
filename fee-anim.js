let isTravelling = false;
let isDancing = false;
let feeElement = null

function handleHover(){
    if(!isTravelling){
        if(!feeElement) feeElement = document.querySelector("#fee")
        feeElement.style.animation = "travel 12s linear 1";
        isTravelling = true;
        setTimeout(() => {
            isTravelling = false
            feeElement.style.animation = "souffle 10s infinite";
        }, 12000);
    }
}

function handleDance(){
    if(!isDancing){
        const feeElement = document.querySelector("#fee")
        feeElement.style.animation = "shutupanddance 12s linear 1";
        isTravelling = true;
        setTimeout(() => {
            isTravelling = false
            feeElement.style.animation = "souffle 10s infinite";
        }, 12000);
    }
}

let sponsorsElement = null;
let repositionnedBoolean = false;

function handlerBottom(e){
    if(!sponsorsElement)
        sponsorsElement = document.querySelector("#logos_partenaires");

    if(!repositionnedBoolean && sponsorsElement.getBoundingClientRect().top < window.innerHeight){
        if(!feeElement) feeElement = document.querySelector("#fee")
        feeElement.style.position = "absolute"
        feeElement.style.top = (sponsorsElement.offsetTop - feeElement.children[0].offsetHeight - 10) + "px"
        repositionnedBoolean = true;
    }
    else if(repositionnedBoolean && sponsorsElement.getBoundingClientRect().top > window.innerHeight){
        if(!feeElement) feeElement = document.querySelector("#fee")
        feeElement.style.position = "fixed"
        feeElement.style.top = ""
        repositionnedBoolean = false;
    }
}

// On ajoute des listeners qui appelle la méthode maître
addEventListener('DOMContentLoaded', handlerBottom);
addEventListener('load', handlerBottom);
addEventListener('scroll', handlerBottom);
addEventListener('resize', handlerBottom);

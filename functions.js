function resizeIframe(obj) {
  obj.style.height = 0;
  obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
}

function smoothScroll(id){
  document.getElementById(id).scrollIntoView({block: "start", behavior: "smooth"});
}


function show_modal(modal){
  modal.style.top="50%";
  document.getElementById("modal_background").style.display="block";
}

function close_modal(modal){
  modal.style.top="-200%";
  document.getElementById("modal_background").style.display="none";
}

function modal_function(modal_id, int){
  var modal = document.getElementById(modal_id);
  var span = document.getElementsByClassName("close")[int];
  show_modal(modal);
  span.onclick=function(){close_modal(modal);}
  modal.onclick=function(){close_modal(modal);}
}

function toggleBurger(){
  const menu = document.getElementById('burger-menu');
  const nav = document.getElementById('mobile-nav');
  if(menu.classList.contains("selected")){
    menu.classList.remove("selected");
    nav.classList.remove("open")
  }
  else {
    menu.classList.add('selected');
    nav.classList.add('open');
  }
}
